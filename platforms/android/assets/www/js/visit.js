var status;
$(document).ready(function(){
		status = window.navigator.onLine;
		if(status==true){
		    $.ajax({
            		            url: "http://soumazize.alwaysdata.net/lorraine/api/Monument",
            		            type: 'GET',
            		            dataType: 'json',
            		            timeout: 1000,
            		            data:'',
            		            error: function(){
            		            location.reload();
            		            	console.log('error serveur'); },
            		            success: placeInfo
            });
		}
		else{
            $('.message').css('display','block');
		}

});

function placeInfo(data){
	var list = $("#loisir");
	for(i in data["Monuments"]){
		console.log(data['Monuments'][i].image[0].chemin);
		var li = $("<li data-corners='false' data-shadow='false' data-iconshadow='true' data-wrapperels='div' data-icon='arrow-r' data-iconpos='right' data-theme='d'></li>");
		li.attr("class","loisir ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-li-has-thumb ui-first-child ui-last-child ui-btn-up-d");
       var div=$("<div class='ui-btn-inner ui-li'><div>");
       var div2=$("<div></div>");
       div2.attr("class","ui-btn-text")
        var span=$("<span class='ui-icon ui-icon-arrow-r ui-icon-shadow'>&nbsp;</span>");
		var a = $("<a data-transition='flip'></a>");
		a.append();
		a.attr("class","ui-link-inherit");
		a.attr("id",data['Monuments'][i].id);
		a.attr("href","#");
		a.html(data['Monuments'][i].ville+"</br></br>"+data['Monuments'][i].nom);
		img = $("<img/>");
		img.attr("class","ui-li-thumb")
		img.attr("alt", data['Monuments'].name);
		img.attr("src", data['Monuments'][i].image[0].chemin);
		a.prepend(img);
		div2.prepend(a);
		div.prepend(div2);
		div.append(span);
		li.append(div);
		list.append(li);
	}
	$(".ui-link-inherit").click(function(){
		loadHome($(this).attr("id"));
	});	

}


function loadHome(Monument){

	window.location = dirname(location.href)+"/visitdetails.html?id="+Monument;
}

function dirname(path) {
    return path.replace(/\\/g, '/')
        .replace(/\/[^\/]*\/?$/, '');
}
