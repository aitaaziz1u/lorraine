var status;
$(document).ready(function(){
		status = window.navigator.onLine;
		if(status==true){
		    $.ajax({
            		           url: "http://soumazize.alwaysdata.net/lorraine/api/Restaurant/"+$.urlParam("id"),
                               		            type: 'GET',
                               		            dataType: 'json',
                               		            timeout: 1000,
                               		            data:'',
                               		            error: function(){
                               		            location.reload();
                               		            	console.log('error serveur'); },
                               		            success: placeImages
            });
		}
		else{
            $('.message').css('display','block');
		}
     	$("#index").click(function(){
    	 window.location.href=dirname(location.href);
    });
});
function placeImages(data){
	var main = $("#main");
    var h1=$("<h1></h1>");
    h1.text(data["Restaurant"][0].nom);
    main.append(h1);
    var div = $("<div></div>");
    div.text(data["Restaurant"][0].descriptif);
    main.append(div);
     var p=$("<p></p>");
    p.text("Adresse : "+data["Restaurant"][0].adresse);
     main.append(p);
    p=$("<p></p>");
    p.text("Tél : "+data["Restaurant"][0].tel);
     main.append(p);
	var images = data["Restaurant"][0].image;
	for(i in images){
		var image = images[i];
		console.log(image.chemin);
		div = $("<div id='links'></div>");
		a = $("<a data-gallery></a>");
		
		// image
		img = $("<img/>");
		img.attr("class","slide")
		img.attr("alt", "default" )
		img.attr("src", image.chemin )

		// Link
		a.attr("title", "© "+image.title);
		a.attr("href", image.chemin);

		a.append(img);
		div.append(a);

		
		var lastRow = main.find(".row").last();

		if(lastRow.children().length>0){
			if(lastRow.children().length == 3 ){
				var row =$("<div class='row thumbnail'></div>");
				row.append(div);
				main.append(row);
			}
			else if(lastRow.children().length < 3){
				lastRow.append(div);
			}
			
		}
		else{
			var row =$("<div class='row thumbnail'></div>");
			row.append(div);
			main.append(row);
		}
			
	}
}

$.urlParam = function(name){
	    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	    if (results==null){
	       return null;
	    }
	    else{
	       return results[1] || 0;
	    }
}

function dirname(path) {
    return path.replace(/\\/g, '/')
        .replace(/\/[^\/]*\/?$/, '');
}


